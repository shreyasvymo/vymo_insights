#!/usr/bin/env python
# coding: utf-8

# In[1]:


import modin.pandas as pd
import pandas as pd
import numpy as np
import os
import sys
import json
from datetime import datetime
from collections import Counter
import plotly.graph_objs as go
from plotly.offline import plot
from plotly import express as px


# In[2]:


won_states = ['converted']
lost_states = ['lost', 'unmet_lost']
met_states = ['met', 'lost', 'closed', 'converted']


# In[3]:


def get_calls_data(updates_entry):
    num_calls, total_call_dur = 0, 0
    for update in updates_entry:
        update_type = update["type"]
        if update_type == "atc":
            num_calls += 1
            total_call_dur += (update["inputs"][0]["data"]["duration"])/1000 # duration in seconds
    return num_calls, total_call_dur


# In[4]:


def get_meeting_duration(updates_entry, inputs_updates_map, met_states):
    total_meetings = 0
    total_duration = 0
    is_reporting = 0

    needs_to_be_met, last_meet_dur = 0, 0
    all_updates_timestamps = []

    for update in updates_entry:
        update_type = update["type"]
        all_updates_timestamps.append(datetime.strptime(update["date"]["$date"],
                                                            "%Y-%m-%dT%H:%M:%S.%fZ").timestamp())
        # Check needs_to_be_met
        if needs_to_be_met == 1 and update_type in met_states:
            total_meetings += 1
            total_duration += max(0, last_meet_dur)
            # reset them for next meeting
            needs_to_be_met, last_meet_dur = 0, 0

        try:
            meeting = inputs_updates_map[update_type]["meeting"]
            last_meet_dur = float(meeting["duration"])/1000
            needs_to_be_met = 1
        except KeyError:
            pass

    all_updates_timestamps = sorted(all_updates_timestamps)
    if all_updates_timestamps[-1] - all_updates_timestamps[0] < 600000 and len(all_updates_timestamps) > 1:
        is_reporting = 1

    return total_duration, total_meetings, is_reporting


# In[5]:


def get_all_fields(json_object):
    df_new_row = {}
    try:
        df_new_row["user_id"] = json_object["user"]["code"]
        df_new_row["code"] = json_object["code"]
    except KeyError:
        df_new_row = {}
        return
    try:
        df_new_row["customer_gender"] = json_object["inputs_map"]["gender"]
    except KeyError:
        pass
    try:
        df_new_row["customer_age"] = json_object["inputs_map"]["age"]
    except KeyError:
        pass
    try:
        df_new_row["customer_city"] = json_object["inputs_map"]["city"]
    except KeyError:
        pass
    try:
        df_new_row["first_update_type"] = json_object["first_update_type"]
    except KeyError:
        pass

    if json_object["last_update_type"] in won_states:
        df_new_row["is_converted"] = 1
        b = 1
    elif json_object["last_update_type"] in lost_states:
        df_new_row["is_converted"] = 0
    else:
        # Neither converted nor dropped
        df_new_row["is_converted"] = -1


    # Get the number of meetings per lead
    total_duration, num_meetings, is_reporting = get_meeting_duration(json_object["updates"], json_object["updates_inputs_map"],
                                                               met_states)
    df_new_row["is_reporting"] = is_reporting
    df_new_row["num_meetings"] = num_meetings
    df_new_row["meeting_duration"] = total_duration/3600

    num_calls, calls_dur = get_calls_data(json_object["updates"])
    df_new_row["number_calls"] = num_calls
    df_new_row["call_duration"] = calls_dur

    return df_new_row


# In[6]:


all_entries = []
file_path = "/home/shreyas/CR-10942/CR-10942_baxa_dump.json"

# with open(file_path) as json_data:
#     for line in json_data:
#         entry = json.loads(line)
#         entry = get_all_fields(entry)
#         if entry:
#             all_entries.append(entry)
#     df = pd.DataFrame.from_dict(all_entries)
#     df.to_csv("baxa_leads_df.csv", index=False)


# In[7]:


df = pd.read_csv("baxa_leads_df.csv")
df[df["number_calls"]!=0].head(2)


# In[8]:


filtered_df = df[(df["first_update_type"] == "new") & (df["is_converted"].isin([1, 0]))]
filtered_df.dropna(inplace=True)
filtered_df.count()


# In[9]:


t1_cities = ["Bangalore", "Chennai", "Delhi", "Hyderabad", "Kolkata", "Mumbai", "Ahmedabad", "Pune"]
t2_cities = ["Agra", "Ajmer", "Aligarh", "Amravati", "Amritsar", "Asansol", "Aurangabad", "Bareilly", "Belgaum", "Bhavnagar", "Bhiwandi", "Bhopal", "Bhubaneswar", "Bikaner", "Bilaspur", "Bokaro Steel City", "Chandigarh", "Coimbatore", "Nagpur", "Cuttack", "Dehradun", "Dhanbad", "Bhilai", "Durgapur", "Erode", "Faridabad", "Firozabad", "Ghaziabad", "Gorakhpur", "Gulbarga", "Guntur", "Gwalior", "Gurgaon", "Guwahati", "Hubli–Dharwad", "Indore", "Jabalpur", "Jaipur", "Jalandhar", "Jammu", "Jamnagar", "Jamshedpur", "Jhansi", "Jodhpur", "Kakinada", "Kannur", "Kanpur", "Kochi", "Kottayam", "Kolhapur", "Kollam", "Kota", "Kozhikode", "Kurnool", "Ludhiana", "Lucknow", "Madurai", "Malappuram", "Mathura", "Goa", "Mangalore", "Meerut", "Moradabad", "Mysore", "Nanded", "Nashik", "Nellore", "Noida", "Palakkad", "Patna", "Pondicherry", "Purulia", "Allahabad", "Raipur", "Rajkot", "Rajahmundry", "Ranchi", "Rourkela", "Salem", "Sangli", "Siliguri", "Solapur", "Srinagar", "Thiruvananthapuram", "Thrissur", "Tiruchirappalli", "Tirupati", "Tirunelveli", "Tiruppur", "Tiruvannamalai", "Ujjain", "Bijapur", "Vadodara", "Varanasi", "Vasai-Virar City", "Vijayawada", "Vellore", "Warangal", "Surat", "Visakhapatnam"]
def bin_customer_city(city):
    city = str(city).title()
    if city in t1_cities:
        return 1
    elif city in t2_cities:
        return 2
    else:
        return 3

filtered_df["customer_city"] = filtered_df.apply(lambda x: bin_customer_city(x["customer_city"]), axis=1)
Counter(filtered_df["customer_city"])


# In[10]:


# Replace Customer gender with their bin equivalent
def bin_cust_gender(gender):
    gender = str(gender).lower()
    if gender in ["male", "m"]:
        return 1
    elif gender in ["female", "f"]:
        return 0

filtered_df["customer_gender"] = filtered_df.apply(lambda row: bin_cust_gender(row["customer_gender"]), axis=1)
Counter(filtered_df["customer_gender"])


# In[11]:


# Replace Customer age with their bin equivalent

filtered_df["customer_age_bin"] = np.digitize(filtered_df["customer_age"], [30, 45, 60]) + 1
Counter(filtered_df["customer_age_bin"])


# In[12]:


# Repalce num_meetings with its bin equivalent
filtered_df["num_meetings_bin"] = np.digitize(filtered_df["num_meetings"], [1, 2, 3, 4, 5])
Counter(filtered_df["num_meetings_bin"])


# In[13]:


# Repalce number_calls with its bin equivalent
filtered_df["number_calls_bin"] = np.digitize(filtered_df["number_calls"], [1, 2, 3, 4, 5])
Counter(filtered_df["number_calls_bin"])

a = [0, 3, 2, 4, 5, 6, 12]
np.digitize(a, [1, 2, 3, 4, 5])


# In[14]:


print(max(filtered_df["meeting_duration"]), min(filtered_df["meeting_duration"]))
print(max(filtered_df["call_duration"]), min(filtered_df["call_duration"]))


# In[15]:


# Repalce meeting_duration with its bin equivalent

filtered_df["meeting_duration_bin"] = np.digitize(filtered_df["meeting_duration"], [0, 1, 2, 4])
Counter(filtered_df["meeting_duration_bin"])


# In[16]:


# Repalce call duration with its bin equivalent
filtered_df["call_duration_bin"] = np.digitize(filtered_df["call_duration"], [1, 120, 300, 1200])
Counter(filtered_df["call_duration_bin"])


# In[ ]:





# In[17]:


# Plot Bar Chart with Gender and Age as attribute

x_axis = ["Male", "Female"]
converted_df = filtered_df[filtered_df["is_converted"]==1]
male_age_count = dict(Counter(converted_df[converted_df["customer_gender"] == 1]["customer_age_bin"]))
female_age_count = dict(Counter(converted_df[converted_df["customer_gender"] == 0]["customer_age_bin"]))
fig = go.Figure(
        data=[
            go.Bar(name='Age: Upto 30', x=x_axis, y=[male_age_count[1], female_age_count[1]],
                   marker=dict(color='rgb(255, 189, 195)')),
            go.Bar(name='Age: 30 to 45', x=x_axis, y=[male_age_count[2], female_age_count[2]],
                   marker=dict(color='rgb(255, 145, 155)')),
            go.Bar(name='Age: 45 to 60', x=x_axis, y=[male_age_count[3], female_age_count[3]],
                   marker=dict(color='rgb(247, 77, 92)')),
            go.Bar(name='Age: 60 and above', x=x_axis, y=[male_age_count[4], female_age_count[4]],
                   marker=dict(color='rgb(250, 5, 38)'))
        ],
        layout=go.Layout(
            title="Customer Age, Gender conversions",
            xaxis={'title': 'Customer Gender'},
            yaxis={'title': 'Number of Converted Customers'}
        )
    )
fig


# In[18]:


# Plot Bar Chart with Number of calls and Call duration as attribute

converted_df = filtered_df[filtered_df["is_converted"]==1]
x_axis = ["<2 minutes", "2-5 minutes", "5-20 minutes", ">20 minutes"]
lt_10_min_count = dict(Counter(converted_df[converted_df["call_duration_bin"] == 1]["number_calls_bin"]))
upto_20_min_count = dict(Counter(converted_df[converted_df["call_duration_bin"] == 2]["number_calls_bin"]))
upto_30_min_count = dict(Counter(converted_df[converted_df["call_duration_bin"] == 3]["number_calls_bin"]))
gt_30_min_count = dict(Counter(converted_df[converted_df["call_duration_bin"] == 4]["number_calls_bin"]))

print(lt_10_min_count, upto_20_min_count, upto_30_min_count, gt_30_min_count)

# Add missing keys to dict
for k in range(1,6):
    if k not in lt_10_min_count.keys():
        lt_10_min_count[k] = 0
    if k not in upto_20_min_count.keys():
        upto_20_min_count[k] = 0
    if k not in upto_30_min_count.keys():
        upto_30_min_count[k] = 0
    if k not in gt_30_min_count.keys():
        gt_30_min_count[k] = 0

fig = go.Figure(
        data=[
            go.Bar(name='# Calls: 1', x=x_axis,
                   y=[lt_10_min_count[1], upto_20_min_count[1], upto_30_min_count[1], gt_30_min_count[1]],
                   marker=dict(color='rgb(255, 189, 195)')),
            go.Bar(name='# Calls: 2', x=x_axis,
                   y=[lt_10_min_count[2], upto_20_min_count[2], upto_30_min_count[2], gt_30_min_count[2]],
                   marker=dict(color='rgb(255, 145, 155)')),
            go.Bar(name='# Calls: 3', x=x_axis,
                   y=[lt_10_min_count[3], upto_20_min_count[3], upto_30_min_count[3], gt_30_min_count[3]],
                   marker=dict(color='rgb(247, 77, 92)')),
            go.Bar(name='# Calls: 4', x=x_axis,
                   y=[lt_10_min_count[4], upto_20_min_count[4], upto_30_min_count[4], gt_30_min_count[4]],
                   marker=dict(color='rgb(250, 5, 38)')),
            go.Bar(name='# Calls: 5 and above', x=x_axis,
                   y=[lt_10_min_count[5], upto_20_min_count[5], upto_30_min_count[5], gt_30_min_count[5]],
                   marker=dict(color='rgb(194, 0, 17)'))
        ],
        layout=go.Layout(
            title="# Calls, Call Durations v/s Lead Conversions",
            xaxis={'title': 'Call Duration'},
            yaxis={'title': 'Number of Converted Customer'}
        )
    )
fig


# In[19]:


# Plot Bar Chart with Number of Meetings and Meeting duration as attribute

converted_df = filtered_df[filtered_df["is_converted"]==1]
x_axis = ["<1 hour", "1-2 hours", "2-4 hours", ">4 hours"]
lt_1_hr_count = dict(Counter(converted_df[converted_df["meeting_duration_bin"] == 1]["num_meetings_bin"]))
upto_2_hrs_count = dict(Counter(converted_df[converted_df["meeting_duration_bin"] == 2]["num_meetings_bin"]))
upto_4_hrs_count = dict(Counter(converted_df[converted_df["meeting_duration_bin"] == 3]["num_meetings_bin"]))
gt_4_hrs_count = dict(Counter(converted_df[converted_df["meeting_duration_bin"] == 4]["num_meetings_bin"]))

print(lt_1_hr_count, upto_2_hrs_count, upto_4_hrs_count, gt_4_hrs_count)

# Add missing keys to dict
for k in range(1,6):
    if k not in lt_1_hr_count.keys():
        lt_1_hr_count[k] = 0
    if k not in upto_2_hrs_count.keys():
        upto_2_hrs_count[k] = 0
    if k not in upto_4_hrs_count.keys():
        upto_4_hrs_count[k] = 0
    if k not in gt_4_hrs_count.keys():
        gt_4_hrs_count[k] = 0

fig = go.Figure(
        data=[
            go.Bar(name='# Meetings: 1', x=x_axis,
                   y=[lt_1_hr_count[1], upto_2_hrs_count[1], upto_4_hrs_count[1], gt_4_hrs_count[1]],
                   marker=dict(color='rgb(255, 189, 195)')),
            go.Bar(name='# Meetings: 2', x=x_axis,
                   y=[lt_1_hr_count[2], upto_2_hrs_count[2], upto_4_hrs_count[2], gt_4_hrs_count[2]],
                   marker=dict(color='rgb(255, 145, 155)')),
            go.Bar(name='# Meetings: 3', x=x_axis,
                   y=[lt_1_hr_count[3], upto_2_hrs_count[3], upto_4_hrs_count[3], gt_4_hrs_count[3]],
                   marker=dict(color='rgb(247, 77, 92)')),
            go.Bar(name='# Meetings: 4', x=x_axis,
                   y=[lt_1_hr_count[4], upto_2_hrs_count[4], upto_4_hrs_count[4], gt_4_hrs_count[4]],
                   marker=dict(color='rgb(250, 5, 38)')),
            go.Bar(name='# Meetings: 5 and above', x=x_axis,
                   y=[lt_1_hr_count[5], upto_2_hrs_count[5], upto_4_hrs_count[5], gt_4_hrs_count[5]],
                   marker=dict(color='rgb(194, 0, 17)'))
        ],
        layout=go.Layout(
            title="# Meetings, Meeting Durations v/s Lead Conversions",
            xaxis={'title': 'Meeting Duration'},
            yaxis={'title': 'Number of Converted Customer'}
        )
    )
fig


# In[20]:


Counter(converted_df["meeting_duration_bin"])


# In[ ]:





# In[ ]:





# ## Run Decision Tree
# ### Parameters to be considered for DT are as follows:
# 1. Customer Gender - Profile Parameter
# 2. Customer Age (Categorical) - Profile Parameter
# 3. Customer City (Categorical) - Profile Parameter
# 4. Number of meeting (Categorical) - User attribute
# 5. Meeting Duration (Categorical) - User attribute
# 6. Number of Calls (Categorical) - User attribute
# 7. Call Duration (Categorical) - User attribute

# In[21]:


from sklearn.tree import DecisionTreeClassifier # Import Decision Tree Classifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split # Import train_test_split function
from sklearn import metrics #Import scikit-learn metrics module for accuracy calculation


# In[22]:


#split dataset in features and target variable
feature_cols = ["customer_age", "customer_gender", "customer_city", "num_meetings", "meeting_duration",
                "number_calls", "call_duration"]
filtered_df.dropna(inplace=True)
X = filtered_df[feature_cols] # Features
y = filtered_df["is_converted"].astype(str) # Target variable
# Split dataset into training set and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1) # 80% training and 20% test

# Create Decision Tree classifer object
clf = RandomForestClassifier(n_estimators=100, max_depth=2, random_state=0)
# Train Decision Tree Classifer
clf = clf.fit(X_train,y_train)

#Predict the response for test dataset
y_pred = clf.predict(X_test)

# Model Accuracy, how often is the classifier correct?
print("Accuracy:", metrics.accuracy_score(y_test, y_pred))
print("Feature Importance", feature_cols, clf.feature_importances_)


# In[24]:


# Visualize the DT
from sklearn.tree import export_graphviz
from sklearn.externals.six import StringIO
from IPython.display import Image
import pydotplus

dot_data = StringIO()
export_graphviz(clf.estimators_[12], out_file=dot_data,
                filled=True, rounded=True,
                feature_names=feature_cols,
                class_names=["0", "1"])
graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
graph.write_png('dt3.png')
Image(graph.create_png())

