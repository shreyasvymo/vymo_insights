import pandas as pd
import os
import json
import sys
from datetime import datetime
import pytz
from plotly import graph_objs as go
from plotly.offline import plot
from collections import Counter
from geopy import distance

root_path = os.path.abspath(os.path.join(os.path.realpath(__file__), "../"))
sys.path.append(root_path)


def create_device_events_df():
    location_df = pd.DataFrame()
    path_to_device_events_df = "/home/shreyas/CR-10315/CR-10315_apollomunich_deviceevents.json"
    db_id, user_codes, latitudes, longitudes, timestamps, time_slots, ping_day, events = [], [], [], [], [], [], [], []

    with open(path_to_device_events_df) as device_events:
        for ind, line in enumerate(device_events):
            print("Iteration num\r", ind, end=" ")
            doc = json.loads(line)
            # Add entries
            user_codes.append(doc["user"])
            db_id.append(doc["_id"])
            try:
                utc_timestamp = doc["location"]["timestamp"]
                completed_date_utc = datetime.utcfromtimestamp(utc_timestamp // 1000).replace(tzinfo=pytz.utc)
                completed_date_ist = completed_date_utc.astimezone(pytz.timezone('Asia/Calcutta'))
                timestamps.append(completed_date_ist.timestamp())
                time_slots.append(completed_date_ist.hour)
                ping_day.append(completed_date_ist.strftime("%m/%d/%Y"))
            except KeyError:
                timestamps.append(None)
                time_slots.append(None)
                ping_day.append(None)

            try:
                assert doc["data"][0]["value"] != "Off"
                latitudes.append(doc["data"][0]["value"]["latitude"])
                longitudes.append(doc["data"][0]["value"]["longitude"])
                events.append(1)
            except KeyError:
                longitudes.append(None)
                latitudes.append(None)
                events.append(None)
            except AssertionError:
                events.append(0)
                longitudes.append(None)
                latitudes.append(None)

    location_df["db_id"] = db_id
    location_df["user_code"] = user_codes
    location_df["latitude"] = latitudes
    location_df["longitude"] = longitudes
    location_df["timestamp"] = timestamps
    location_df["time_slot"] = time_slots
    location_df["ping_day"] = ping_day
    location_df["event"] = events
    return location_df


def create_user_location_df():
    dataframe = pd.DataFrame()
    path_to_user_location = "/home/shreyas/CR-10315/CR-10315_apollomunich_userlocations.json"
    db_id, user_codes, latitudes, longitudes, timestamps, time_slots, ping_day = [], [], [], [], [], [], []

    # Append persistency lead data to Leads table
    with open(path_to_user_location) as user_locations:

        for ind, line in enumerate(user_locations):
            print("Iteration num\r", ind, end=" ")
            doc = json.loads(line)
            try:
                db_id.append(doc["_id"])
            except KeyError:
                db_id.append(None)

            try:
                user_codes.append(doc["user"]["code"])
            except KeyError:
                user_codes.append(None)

            try:
                latitudes.append(doc["location"]["latitude"])
            except KeyError:
                latitudes.append(None)

            try:
                longitudes.append(doc["location"]["longitude"])
            except KeyError:
                longitudes.append(None)

            try:
                utc_timestamp = doc["location"]["timestamp"]
                completed_date_utc = datetime.utcfromtimestamp(utc_timestamp // 1000).replace(tzinfo=pytz.utc)
                completed_date_ist = completed_date_utc.astimezone(pytz.timezone('Asia/Calcutta'))
                timestamps.append(completed_date_ist.timestamp())
                time_slots.append(completed_date_ist.hour)
                ping_day.append(completed_date_ist.day)
            except KeyError:
                timestamps.append(None)
                time_slots.append(None)
                ping_day.append(None)

        dataframe["db_id"] = db_id
        dataframe["user_code"] = user_codes
        dataframe["latitude"] = latitudes
        dataframe["longitude"] = longitudes
        dataframe["timestamp"] = timestamps
        dataframe["time_slot"] = time_slots
        dataframe["ping_day"] = ping_day
    return dataframe


def create_calender_items_df():
    dataframe = pd.DataFrame()
    path_to_calender_items = "/home/shreyas/CR-10315/CR-10315_apollomunich_calendar_items.json"
    db_id, user_codes, creation_dates, completion_dates, ce_types, partner_codes = [], [], [], [], [], []
    start_day, end_day, completion_slots, creation_slots = [], [], [], []

    # Append persistency lead data to Leads table
    with open(path_to_calender_items) as per_leads_f:

        for ind, line in enumerate(per_leads_f):
            print("Lead num\r", ind, end="")
            doc = json.loads(line)
            try:
                db_id.append(doc["_id"])
            except KeyError:
                db_id.append(None)

            try:
                user_codes.append(doc["data"]["user"]["code"])
            except KeyError:
                user_codes.append(None)

            try:
                creation_date_utc = datetime.strptime(doc["created"]["server_date"]["$date"], "%Y-%m-%dT%H:%M:%S.%fZ")
                creation_date_utc = datetime.utcfromtimestamp(creation_date_utc.timestamp()).replace(tzinfo=pytz.utc)
                creation_date_ist = creation_date_utc.astimezone(pytz.timezone('Asia/Calcutta'))
                creation_dates.append(creation_date_ist.timestamp())
                creation_slots.append(creation_date_ist.hour)
                start_day.append(creation_date_ist.day)
            except KeyError:
                creation_dates.append(None)
                creation_slots.append(None)
                start_day.append(None)

            try:
                completed_date_utc = datetime.strptime(doc["completed"]["start_date"]["$date"], "%Y-%m-%dT%H:%M:%S.%fZ")
                completed_date_utc = datetime.utcfromtimestamp(completed_date_utc.timestamp()).replace(tzinfo=pytz.utc)
                completed_date_ist = completed_date_utc.astimezone(pytz.timezone('Asia/Calcutta'))
                completion_dates.append(completed_date_ist.timestamp())
                completion_slots.append(completed_date_ist.hour)
                end_day.append(completed_date_ist.day)
            except KeyError:
                completion_dates.append(None)
                completion_slots.append(None)
                end_day.append(None)

            try:
                ce_types.append(doc["category"])
            except KeyError:
                ce_types.append(None)

            try:
                partner_codes.append(doc["data"]["vo"]["code"])
            except KeyError:
                partner_codes.append(None)

    dataframe["db_id"] = db_id
    dataframe["user_code"] = user_codes
    dataframe["creation_date"] = creation_dates
    dataframe["completion_date"] = completion_dates
    dataframe["creation_slot"] = creation_slots
    dataframe["completion_slot"] = completion_slots
    dataframe["calender_event_type"] = ce_types
    dataframe["partner_code"] = partner_codes
    dataframe["start_day"] = start_day
    dataframe["end_day"] = end_day

    return dataframe


def create_ping_distribution_plot(slot_count_dict):
    all_slots = list(slot_count_dict.keys())
    all_slot_counts = list(slot_count_dict.values())

    fig = go.Figure(
        layout=go.Layout(
            title="Distribution of Pings Hour-wise",
            xaxis={'title': 'Hour Bucket'},
            yaxis={'title': 'Total number of pings.'}
        ))
    fig.add_trace(go.Bar(x=all_slots, y=all_slot_counts))

    plot(fig, filename=os.path.join("pings_distribution.html"))
    return


def create_creation_time_distribution_plot(slot_count_dict):
    all_slots = list(slot_count_dict.keys())
    all_slot_counts = list(slot_count_dict.values())

    fig = go.Figure(
        layout=go.Layout(
            title="Distribution of Calender event Creation time Hour-wise",
            xaxis={'title': 'Hour Bucket'},
            yaxis={'title': 'Total number of events created.'}
        ))
    fig.add_trace(go.Bar(x=all_slots, y=all_slot_counts))

    plot(fig, filename=os.path.join("creation_time_slot_distribution.html"))
    return


def create_completion_time_distribution_plot(slot_count_dict):
    all_slots = list(slot_count_dict.keys())
    all_slot_counts = list(slot_count_dict.values())

    fig = go.Figure(
        layout=go.Layout(
            title="Distribution of Calender event Completion time Hour-wise",
            xaxis={'title': 'Hour Bucket'},
            yaxis={'title': 'Total number of events completed.'}
        ))
    fig.add_trace(go.Bar(x=all_slots, y=all_slot_counts))

    plot(fig, filename=os.path.join("completion_time_slot_distribution.html"))
    return


def filter_unique_location_pings(data_frame):
    # The technique for filtering data from the df is by sorting data and then removing those entries
    # that are less than 50 meter apart from the previous entries and 20 minutes
    filtered_data_frame = pd.DataFrame()
    db_id, user_codes, latitudes, longitudes, timestamps, time_slots, ping_day = [], [], [], [], [], [], []
    for ind, row in data_frame.iterrows():
        print("Row \r {}".format(ind), end="")
        if len(db_id) == 0:
            db_id.append(row["db_id"])
            user_codes.append(row["user_code"])
            latitudes.append(row["latitude"])
            longitudes.append(row["longitude"])
            timestamps.append(row["timestamp"])
            time_slots.append(row["time_slot"])
            ping_day.append(row["ping_day"])
            filtered_time, filtered_user = row["timestamp"], row["user_code"]
            filtered_lat, filtered_long = row["latitude"], row["longitude"]
            continue

        dist = distance.distance((filtered_lat, filtered_long), (row["latitude"], row["longitude"]))
        if dist > 0.05 or filtered_user != row["user_code"] or (row["timestamp"] - filtered_time) > 120:
            db_id.append(row["db_id"])
            user_codes.append(row["user_code"])
            latitudes.append(row["latitude"])
            longitudes.append(row["longitude"])
            timestamps.append(row["timestamp"])
            time_slots.append(row["time_slot"])
            ping_day.append(row["ping_day"])
            filtered_time, filtered_user = row["timestamp"], row["user_code"]
            filtered_lat, filtered_long = row["latitude"], row["longitude"]

    filtered_data_frame["db_id"] = db_id
    filtered_data_frame["user_code"] = user_codes
    filtered_data_frame["latitude"] = latitudes
    filtered_data_frame["longitude"] = longitudes
    filtered_data_frame["timestamp"] = timestamps
    filtered_data_frame["time_slot"] = time_slots
    filtered_data_frame["ping_day"] = ping_day
    return filtered_data_frame


def group_users_by_pings(pings_data_frame):
    df = pings_data_frame.groupby(['user_code', 'ping_day']).agg({'user_code': 'first', 'ping_day': 'first',
                                                                  'latitude': 'count'})
    df.rename(columns={'latitude': 'ping_count'}, inplace=True)
    df.to_csv("user_day_wise_pings.csv", index=False)
    df = df.add_suffix('_').reset_index()
    df = df[["user_code", "ping_day", "ping_count_"]]
    return df.groupby(['user_code']).agg({'user_code': 'first', 'ping_count_': 'mean'})


if __name__ == '__main__':
    events_df = create_device_events_df()
    exit()
    calender_events_df = create_calender_items_df()
    valid_calender_df = calender_events_df[(calender_events_df["calender_event_type"] == "DETECT_CALENDAR_ITEM") | (
            calender_events_df["calender_event_type"] == "VO_CALENDAR_ITEM")]
    valid_calender_df.to_csv("calender_events.csv", index=False)
    valid_calender_df = pd.read_csv("calender_events.csv")

    user_locations_df = create_user_location_df()
    user_locations_df.sort_values(by=["timestamp"], inplace=True)
    user_locations_df.to_csv("user_locations.csv", index=False)

    user_locations_df = pd.read_csv("user_locations.csv")
    filtered_user_loc = filter_unique_location_pings(user_locations_df)
    filtered_user_loc.to_csv("filtered_user_pings.csv", index=False)
    filtered_user_loc = pd.read_csv("filtered_user_pings.csv", low_memory=False)
    user_ping_count = group_users_by_pings(filtered_user_loc)
    user_ping_count.to_csv("user_ping_count.csv", index=False)

    create_ping_distribution_plot(Counter(filtered_user_loc["time_slot"]))

    vo_calender_df = valid_calender_df[valid_calender_df["calender_event_type"] == "VO_CALENDAR_ITEM"]
    user_level_df_vo = vo_calender_df.groupby(["user_code", "end_day"]).agg({"user_code": "first", "end_day": "first",
                                                                             "partner_code": "count"})
    user_level_df_vo = user_level_df_vo.add_suffix('_').reset_index()
    user_level_df_vo = user_level_df_vo[["user_code", "end_day", "partner_code_"]]
    user_level_df_vo.rename(columns={"end_day": "day", "partner_code_": "num_act_completed"}, inplace=True)

    create_completion_time_distribution_plot(Counter(vo_calender_df["completion_slot"]))

    detect_calender_df = valid_calender_df[valid_calender_df["calender_event_type"] == "DETECT_CALENDAR_ITEM"]
    user_level_df_detect = detect_calender_df.groupby(["user_code", "start_day"]).agg({"user_code": "first",
                                                                                       "start_day": "first",
                                                                                       "partner_code": "count"})
    user_level_df_detect = user_level_df_detect.add_suffix('_').reset_index()
    user_level_df_detect = user_level_df_detect[["user_code", "start_day", "partner_code_"]]
    user_level_df_detect.rename(columns={"start_day": "day", "partner_code_": "num_act_detected"}, inplace=True)

    user_level_df = pd.merge(user_level_df_detect, user_level_df_vo, on=['user_code', 'day'])
    pings_df = pd.read_csv("user_day_wise_pings.csv")
    pings_df.rename(columns={"ping_day": "day"}, inplace=True)
    user_level_df = pd.merge(user_level_df, pings_df, on=['user_code', 'day'])

    user_level_df.to_csv("user_day_level_analysis.csv", index=False)
    create_creation_time_distribution_plot(Counter(detect_calender_df["creation_slot"]))

