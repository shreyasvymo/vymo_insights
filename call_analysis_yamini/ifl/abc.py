import pandas as pd
import json
from datetime import datetime
import pytz
import numpy as np
from plotly import graph_objs as go
from plotly.offline import plot
from collections import Counter


def create_device_events_df():
    location_df = pd.DataFrame()
    path_to_device_events_df = "/home/shreyas/CR-8185/indiafirstlife_leads_persistency_new_CR-8185.json"
    db_id, user_codes, due_dates, atc_timestamps, first_atc, converted = [], [], [], [], [], []

    with open(path_to_device_events_df) as device_events:
        for ind, line in enumerate(device_events):
            print("Iteration num\r", ind, end=" ")
            doc = json.loads(line)
            # Add entries
            user_codes.append(doc["user"]["code"])
            db_id.append(doc["_id"])
            # Add policy due date
            policy_due_date = doc["updates_inputs_map"]["persistency_new"]["due_date"]
            completed_date_utc = datetime.utcfromtimestamp(policy_due_date // 1000).replace(tzinfo=pytz.utc)
            completed_date_ist = completed_date_utc.astimezone(pytz.timezone('Asia/Calcutta'))
            due_dates.append(completed_date_ist.timestamp())

            # get the data for atc
            # iterate_through_updates and get the atc call time
            atc_timestamps_row = []
            is_converted = -1
            for update in doc["updates"]:
                if update["type"] == "atc":
                    atc_date_utc = datetime.strptime(update["date"]["$date"], "%Y-%m-%dT%H:%M:%S.%fZ")
                    atc_date_utc = datetime.utcfromtimestamp(atc_date_utc.timestamp()).replace(
                        tzinfo=pytz.utc)
                    atc_date_ist = atc_date_utc.astimezone(pytz.timezone('Asia/Calcutta'))
                    atc_timestamps_row.append(atc_date_ist.timestamp())
                elif update["type"] == "persistency_converted":
                    is_converted = 1
                elif update["type"] == "persistency_cancelled":
                    is_converted = 0
            try:
                all_timestamps = sorted(atc_timestamps_row)
                first_atc.append(all_timestamps[0])
                atc_timestamps.append(str(all_timestamps))
            except IndexError:
                first_atc.append(None)
                atc_timestamps.append(None)

            converted.append(is_converted)
    location_df["db_id"] = db_id
    location_df["user_code"] = user_codes
    location_df["due_date"] = due_dates
    location_df["first_atc"] = first_atc
    location_df["is_converted"] = converted
    location_df["atc_timestamps"] = atc_timestamps
    return location_df


def myround(x, base=5):
    rounded_x = []
    for i in x:
        rounded_x.append(base * round(i/base))
    return rounded_x


def plot_histogram(x):
    print(x)
    trace = go.Histogram(x=x, xbins=dict(start=np.min(x), size=0.05, end=np.max(x)))

    layout = go.Layout(
        title="Converted Frequency Counts"
    )

    fig = go.Figure(data=trace, layout=layout)
    plot(fig, filename='histogram-freq-counts.html')


if __name__ == '__main__':
    data_frame = create_device_events_df()
    data_frame["td"] = (data_frame["due_date"] - data_frame["first_atc"])/(24*3600)
    data_frame.to_csv("ifl_persistency_atc_timestamps.csv", index=False)

    # Get plots of converted leads and dropped leads
    converted_leads = data_frame[data_frame["is_converted"] == 1].dropna()
    valid_converted_leads = converted_leads[converted_leads["td"] > 0]
    valid_converted_leads["rounded_td"] = np.clip(myround(valid_converted_leads["td"].to_list()), a_min=0, a_max=40)
    valid_converted_leads.to_csv("converted_leads.csv", index=False)
    print(np.mean(valid_converted_leads["td"]), np.median(valid_converted_leads["td"]))
    print(Counter(valid_converted_leads["rounded_td"].to_list()))
    num_calls = [len(eval(x)) for x in valid_converted_leads["atc_timestamps"].to_list()]
    print("Converted avg calls")
    print(np.mean(num_calls), np.median(num_calls))
    # plot_histogram(valid_converted_leads["rounded_td"].to_list())

    dropped_leads = data_frame[data_frame["is_converted"] == 0]
    num_calls = [len(eval(x)) for x in dropped_leads["atc_timestamps"].to_list()]
    print("Dropped calls")
    print(num_calls)
    dropped_leads = dropped_leads.dropna()
    valid_dropped_leads = dropped_leads[dropped_leads["td"] > 0]
    valid_dropped_leads["rounded_td"] = np.clip(myround(valid_dropped_leads["td"].to_list()), a_max=40, a_min=0)
    valid_dropped_leads.to_csv("dropped_leads.csv", index=False)
    print(np.mean(valid_dropped_leads["td"]), np.median(valid_dropped_leads["td"]))
    num_calls = [len(eval(x)) for x in valid_dropped_leads["atc_timestamps"].to_list()]
    print("Dropped calls")
    print(num_calls)
    print(np.mean(num_calls), np.median(num_calls))
    # plot_histogram(dropped_leads["rounded_td"].to_list())

