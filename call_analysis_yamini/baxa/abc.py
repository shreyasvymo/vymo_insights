import pandas as pd
import json
from datetime import datetime
import pytz
import numpy as np
from plotly import graph_objs as go
from plotly.offline import plot
from collections import Counter


def create_device_events_df():
    location_df = pd.DataFrame()
    path_to_device_events_df = "/home/shreyas/CR-10434/baxa_leads_CR-10434.json"
    db_id, assigned_date, atc_timestamps, first_atc, converted = [], [], [], [], []
    last_update_type = set()
    count = 0
    with open(path_to_device_events_df) as device_events:
        for ind, line in enumerate(device_events):
            print("Iteration num\r", ind, end=" ")
            doc = json.loads(line)
            if doc["first_update_type"] != "new":
                count += 1
                continue
            # Add entries
            db_id.append(doc["_id"])

            try:
                assigned_date_utc = datetime.strptime(doc["date"]["$date"], "%Y-%m-%dT%H:%M:%S.%fZ")
                assigned_date_utc = datetime.utcfromtimestamp(assigned_date_utc.timestamp()).replace(tzinfo=pytz.utc)
                assigned_date_ist = assigned_date_utc.astimezone(pytz.timezone('Asia/Calcutta'))
                assigned_date.append(assigned_date_ist.timestamp())
            except KeyError:
                assigned_date.append(None)
            last_update_type.add(doc["last_update_type"])

            # get the data for atc
            # iterate_through_updates and get the atc call time
            atc_timestamps_row = []
            is_converted = -1
            for update in doc["updates"]:
                if update["type"] == "atc":
                    atc_date_utc = datetime.strptime(update["date"]["$date"], "%Y-%m-%dT%H:%M:%S.%fZ")
                    atc_date_utc = datetime.utcfromtimestamp(atc_date_utc.timestamp()).replace(
                        tzinfo=pytz.utc)
                    atc_date_ist = atc_date_utc.astimezone(pytz.timezone('Asia/Calcutta'))
                    atc_timestamps_row.append(atc_date_ist.timestamp())

                elif update["type"] == "converted":
                    is_converted = 1
                elif update["type"] in ["lost", "unmet_lost"]:
                    is_converted = 0

            converted.append(is_converted)

            try:
                all_timestamps = sorted(atc_timestamps_row)
                first_atc.append(all_timestamps[0])
                atc_timestamps.append(str(all_timestamps))
            except IndexError:
                first_atc.append(None)
                atc_timestamps.append(None)
    print("All last update states", last_update_type)
    print("Invalid Counts", count)
    location_df["db_id"] = db_id
    location_df["lead_assigned_date"] = assigned_date
    location_df["first_atc"] = first_atc
    location_df["is_converted"] = converted
    location_df["atc_timestamps"] = atc_timestamps
    return location_df


def myround(x, base=5):
    rounded_x = []
    for i in x:
        rounded_x.append(base * round(i/base))
    return rounded_x


def plot_histogram(x, filename):
    trace = go.Histogram(x=x, xbins=dict(start=np.min(x), size=0.5, end=np.max(x)),
                         marker=dict(
                             color='#e31025'
                         ),
                         )

    layout = go.Layout(
        title="First time to Call v/s Leads Converted",
        xaxis={'title': 'First call Hour Bucket'},
        yaxis={'title': 'Number of leads converted'}
    )

    fig = go.Figure(layout=layout)
    fig.add_trace(trace)
    count_dict = Counter(x)
    print(count_dict)
    fig.add_trace(go.Scatter(x=list(count_dict.keys()), y=list(count_dict.values()), line=dict(
        color='#b5b0b0', width=4)))
    plot(fig, filename='{}.html'.format(filename))


if __name__ == '__main__':
    # data_frame = create_device_events_df()
    # data_frame["td"] = (data_frame["first_atc"] - data_frame["lead_assigned_date"])/3600
    # data_frame.to_csv("baxa_leads.csv", index=False)
    #
    # # Get plots of converted leads and dropped leads
    # converted_leads = data_frame[data_frame["is_converted"] == 1]
    # converted_leads.to_csv("converted_leads_baxa.csv", index=False)
    #
    # converted_leads.dropna(inplace=True)
    # valid_converted_leads = converted_leads[converted_leads["td"] > 0]
    # valid_converted_leads["rounded_td"] = np.clip(myround(valid_converted_leads["td"].to_list(), base=1), a_max=10,
    #                                               a_min=0)
    # valid_converted_leads.to_csv("valid_converted_leads_baxa.csv", index=False)
    valid_converted_leads = pd.read_csv("valid_converted_leads_baxa.csv")
    # print(np.mean(valid_converted_leads["td"]), np.median(valid_converted_leads["td"]))
    plot_histogram(valid_converted_leads["rounded_td"].to_list(), "baxa_converted")

    # dropped_leads = data_frame[data_frame["is_converted"] == 0].dropna()
    # valid_dropped_leads = dropped_leads[dropped_leads["td"] > 0]
    # valid_dropped_leads["rounded_td"] = np.clip(myround(valid_dropped_leads["td"].to_list(), base=1), a_max=10, a_min=0)
    # valid_dropped_leads.to_csv("dropped_leads.csv", index=False)
    # # print(np.mean(valid_dropped_leads["td"]), np.median(valid_dropped_leads["td"]))
    # plot_histogram(dropped_leads["rounded_td"].to_list(), "baxa_dropped")


