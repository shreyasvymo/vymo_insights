import pandas as pd
import json
from datetime import datetime
import pytz
import numpy as np
from plotly import graph_objs as go
from plotly.offline import plot


def create_device_events_df():
    location_df = pd.DataFrame()
    path_to_device_events_df = "/home/shreyas/CR-8185/indiafirstlife_leads_persistency_new_CR-8185.json"
    db_id, user_codes, due_dates, atc_timestamps, first_atc, converted = [], [], [], [], [], []

    with open(path_to_device_events_df) as device_events:
        for ind, line in enumerate(device_events):
            print("Iteration num\r", ind, end=" ")
            doc = json.loads(line)
            # Add entries
            user_codes.append(doc["user"]["code"])
            db_id.append(doc["_id"])
            # Add policy due date
            policy_due_date = doc["updates_inputs_map"]["persistency_new"]["due_date"]
            completed_date_utc = datetime.utcfromtimestamp(policy_due_date // 1000).replace(tzinfo=pytz.utc)
            completed_date_ist = completed_date_utc.astimezone(pytz.timezone('Asia/Calcutta'))
            due_dates.append(completed_date_ist.timestamp())

            # get the data for atc
            # iterate_through_updates and get the atc call time
            atc_timestamps_row = []
            is_converted = -1
            for update in doc["updates"]:
                if update["type"] == "atc":
                    atc_date_utc = datetime.strptime(update["date"]["$date"], "%Y-%m-%dT%H:%M:%S.%fZ")
                    atc_date_utc = datetime.utcfromtimestamp(atc_date_utc.timestamp()).replace(
                        tzinfo=pytz.utc)
                    atc_date_ist = atc_date_utc.astimezone(pytz.timezone('Asia/Calcutta'))
                    atc_timestamps_row.append(atc_date_ist.timestamp())
                elif update["type"] == "persistency_converted":
                    is_converted = 1
                elif update["type"] == "persistency_cancelled":
                    is_converted = 0
            try:
                all_timestamps = sorted(atc_timestamps_row)
                first_atc.append(all_timestamps[0])
                atc_timestamps.append(str(all_timestamps))
            except IndexError:
                first_atc.append(None)
                atc_timestamps.append(None)

            converted.append(is_converted)
    location_df["db_id"] = db_id
    location_df["user_code"] = user_codes
    location_df["due_date"] = due_dates
    location_df["first_atc"] = first_atc
    location_df["is_converted"] = converted
    location_df["atc_timestamps"] = atc_timestamps
    return location_df


def myround(x, base=5):
    rounded_x = []
    for i in x:
        rounded_x.append(base * round(i/base))
    return rounded_x


def plot_histogram(x):
    print(x)
    trace = go.Histogram(x=x, xbins=dict(start=np.min(x), size=0.05, end=np.max(x)))

    layout = go.Layout(
        title="Histogram Frequency Counts"
    )

    fig = go.Figure(data=go.Data([trace]), layout=layout)
    plot(fig, filename='histogram-freq-counts.html')


if __name__ == '__main__':
    data_frame = create_device_events_df()
    data_frame["td"] = (data_frame["due_date"] - data_frame["first_atc"])/(24*3600)
    data_frame.to_csv("ifl_persistency_atc_timestamps.csv", index=False)

    # Get average of converted leads and dropped leads
    converted_leads = data_frame[data_frame["is_converted"] == 1].dropna()
    valid_converted_leads = converted_leads[converted_leads["td"] > 0]

    dropped_leads = data_frame[data_frame["is_converted"] == 0].dropna()
    valid_dropped_leads = dropped_leads[dropped_leads["td"] > 0]

    valid_converted_leads["rounded_td"] = myround(valid_converted_leads["td"].to_list())
    valid_dropped_leads["rounded_td"] = myround(valid_dropped_leads["td"].to_list())

    valid_converted_leads.to_csv("converted_leads.csv", index=False)
    valid_dropped_leads.to_csv("dropped_leads.csv", index=False)

    print(np.mean(valid_converted_leads["td"]), np.median(valid_converted_leads["td"]))
    print(np.mean(valid_dropped_leads["td"]), np.median(valid_dropped_leads["td"]))

    plot_histogram(valid_converted_leads["rounded_td"].to_list())
    # plot_histogram(dropped_leads["rounded_td"].to_list())

