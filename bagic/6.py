import pandas as pd


def get_all_log_in_users(start_day_df):
    groups = start_day_df.groupby(["Created"])
    df = pd.DataFrame()
    for group in groups:
        created_date = group[1]["Created"].to_list()[0]
        login_users = str(group[1]["Assigned To"].to_list())
        df = df.append({"Date": created_date, "Users": login_users}, ignore_index=True)
    return df

        
def main():
    path = "/Users/shreyasrk/Downloads/Activity List.xlsx"
    df = pd.read_excel(path)
    start_day_df = df[df["Activity Name"] == "Start Day"]
    end_day_df = df[df["Activity Name"] == "End Day"]

    all_login_users = get_all_log_in_users(start_day_df)
    all_logout_users = get_all_log_in_users(end_day_df)
    
    all_login_users.to_csv("bagic/all_login_users.csv", index=False)
    all_logout_users.to_csv("bagic/all_logout_users.csv", index=False)


if __name__ == '__main__':
    main()

