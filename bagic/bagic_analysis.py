"""
TODO:
- Filter data by first_update_type = "banca_activity_new"
- Now standardize the data by considering user_code, partner_code, time_stamp
- Groupby (user_code, partner_code)
- for every group check the difference of timestamps.
- if timestamp_diff < 10:
    print (details)
"""

import json
import pandas as pd
from datetime import datetime
from geopy import distance as gd
from dateutil import rrule


def json_load(file_object):
    for ind, line in enumerate(file_object):
        print(ind)
        yield json.loads(line)


def create_calender_data_frame(file_name):
    df = pd.DataFrame()
    with open(file_name) as fo:
        for row in json_load(fo):
            new_entry = {}
            if row["name"] in ["Activity Location Detect", "VO Location Detect"]:
                continue

            try:
                new_entry["branch_lead_code"] = row["data"]["vo"]["code"]
                assert row["data"]["vo"]["vo_module"]["code"] == "branch"
            except KeyError:
                pass
            except AssertionError:
                continue
            try:
                new_entry["user_code"] = row["data"]["user"]["code"]
            except KeyError:
                pass
            try:
                new_entry["calender_code"] = row["code"]
            except KeyError:
                pass
            try:
                new_entry["timestamp"] = datetime.strptime(row["created"]["date"]["$date"],
                                                           "%Y-%m-%dT%H:%M:%S.%fZ").timestamp()
            except KeyError:
                pass
            try:
                for item in row["complete_inputs"]:
                    if item["type"] == "location":
                        loc = json.loads(item["value"])
                        new_entry["location"] = (loc["latitude"], loc["longitude"])
            except KeyError:
                pass
            try:
                new_entry["event_state"] = row["state"]
            except KeyError:
                pass
            try:
                new_entry["activity_code"] = row["data"]["task"]["code"]
            except KeyError:
                pass

            df = df.append(new_entry, ignore_index=True)
    return df


def find_fraudulent_entries(calender_events_df):
    # group_by_user_code and branch_lead_code and check the timestamps of that group
    groups = calender_events_df.groupby(["user_code", "branch_lead_code"])
    df = pd.DataFrame()
    for group in groups:
        # Get all timestamps and check if the timestamps is within 10 minutes
        all_timestamps = sorted(group[1]["timestamp"].to_list())
        all_calender_lc = group[1]["calender_code"].to_list()
        for i in range(len(all_timestamps)):
            for j in range(i + 1, len(all_timestamps)):
                if abs(all_timestamps[i] - all_timestamps[j]) <= 600:
                    print("These two calender events have occurred with 10 minutes window", all_calender_lc[i],
                          all_calender_lc[j])
                    try:
                        user_code = group[1]["user_code"].to_list()[0]
                    except KeyError:
                        user_code = None
                        print(group[1]["user_code"])
                    try:
                        branch_lead_code = group[1]["branch_lead_code"].to_list()[0]
                    except KeyError:
                        branch_lead_code = None
                        print(group[1]["branch_lead_code"])
                    try:
                        activity_code = group[1]["activity_code"].to_list()[0]
                    except KeyError:
                        activity_code = None
                        print(group[1]["activity_code"])

                    df = df.append({"cc1": all_calender_lc[i], "cc2": all_calender_lc[j], "user_code": user_code,
                                    "branch_lead_code": branch_lead_code, "activity_code": activity_code},
                                   ignore_index=True)

    return df


def create_branch_df(branch_file_name):
    df = pd.DataFrame()
    with open(branch_file_name) as fo:
        for row in json_load(fo):
            new_entry = {}
            try:
                assert row["first_update_type"] == "branch_new"
            except AssertionError:
                continue
            try:
                new_entry["branch_lead_code"] = row["code"]
            except KeyError:
                pass

            try:
                new_entry["location"] = (row["location"]["latitude"], row["location"]["longitude"])
            except KeyError:
                pass

            try:
                new_entry["branch_name"] = row["inputs_map"]["name"]
            except KeyError:
                pass

            df = df.append(new_entry, ignore_index=True)
    return df


def activity_location_verification(calender_events_df, branch_lead_code_location_dict):
    df = pd.DataFrame()
    for ind, row in calender_events_df.iterrows():
        if row["event_state"] == "COMPLETED":
            location = row["location"]
            branch_code = row['branch_lead_code']
            try:
                branch_location = branch_lead_code_location_dict[branch_code]
                try:
                    dist = gd.distance(eval(location), eval(branch_location))
                    if dist > 0.5:
                        print(location, branch_location, dist)
                        df = df.append({"calender_code": row["calender_code"], "user_code": row["user_code"],
                                       "branch_code": branch_code, "update_dist": dist}, ignore_index=True)
                    else:
                        print("Distance is", dist)
                except ValueError:
                    pass
                except TypeError:
                    pass

            except KeyError:
                print("Branch id missing", branch_code)
    return df


def branch_productivity_computer(calender_events_df):
    groups = calender_events_df.groupby("branch_lead_code")
    df = pd.DataFrame()
    for group in groups:
        all_timestamps = sorted(group["timestamp"].to_list())
        branch_lead_code = group["branch_lead_code"][0]
        first_date, last_date = all_timestamps[0], all_timestamps[-1]
        diff_business_days = len(list(rrule.rrule(rrule.DAILY, dtstart=first_date,
                                                  until=last_date - datetime.timedelta(days=1),
                                                  byweekday=(rrule.MO, rrule.TU, rrule.WE, rrule.TH, rrule.FR))))
        num_events = len(all_timestamps)
        df = df.append({"branch_lead_code": branch_lead_code, "num_events": num_events,
                        "num_business_days": diff_business_days}, ignore_index=True)

    return groups


def main():
    calender_events_file_name = "/home/ubuntu/CR-8800/bagic_banca_calendar_items_CR-8800.json"
    branch_file_name = "/home/ubuntu/CR-8800/bagic_banca_leads_CR-8800.json"

    # calender_events_df = create_calender_data_frame(calender_events_file_name)
    # calender_events_df.to_csv("bagic/calender_events.csv", index=False)
    calender_events_df = pd.read_csv("bagic/calender_events.csv")
    # fraudulent_entries = find_fraudulent_entries(calender_events_df)
    # fraudulent_entries.to_csv("bagic/fraudulent_entries_calender.csv", index=False)
    # print(fraudulent_entries)

    # branch_df = create_branch_df(branch_file_name)
    # branch_df.to_csv("bagic/branch.csv", index=False)
    branch_df = pd.read_csv("bagic/branch.csv")
    branch_lead_code_location_dict = dict(zip(branch_df["branch_lead_code"].to_list(), branch_df["location"].to_list()))
    location_update_invalid_df = activity_location_verification(calender_events_df, branch_lead_code_location_dict)
    location_update_invalid_df.to_csv("bagic/invalid_location_updates.csv", index=False)
    print(location_update_invalid_df)


if __name__ == '__main__':
    main()

