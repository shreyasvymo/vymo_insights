import pandas as pd
import glob

all_files = glob.glob("./*.tsv")

li = []

for filename in all_files:
    df = pd.read_csv(filename,sep="\t")
    li.append(df)

frame = pd.concat(li, ignore_index=True)
frame.to_csv("all_customers.tsv", sep="\t", index=False)
