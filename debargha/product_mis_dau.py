
import modin.pandas as pd
import numpy as np
import warnings
warnings.filterwarnings('ignore')
import calendar
#import jsonlines
from pprint import pprint
import datetime as dt
import glob
#import delorean
import json
import re
import copy


#
# user_act = pd.read_csv('/home/shreyas/CR-10712/mysql/user_activity_data_CR-10792.csv')
#
# user_act = user_act[~user_act['user_code'].isin({'vymoadmin', 'Vymo Agent',
#                                                  'Vymo Admin', 'Vymomanager',
#                                                  'vymoagent', 'SU','Vymo Agent'})]
#
# mapper = {'1': 'Jan', '2': 'Feb', '3': 'Mar', '4': 'Apr',
#           '5': 'May', '6': 'Jun', '7': 'Jul', '8': 'Aug',
#           '9': 'Sep', '10': 'Oct', '11': 'Nov', '12': 'Dec'}
#
# user_act['date'] = pd.to_datetime(user_act['dates'])
# user_act['month'] = user_act['date'].apply(lambda x: mapper[str(x.month)])
# user_act['year'] = user_act['date'].apply(lambda x: x.year)
#
#
# # In[12]:
#
#
# user_act['read_flag'] = user_act.apply(lambda row: 1 if (row['no_add'] > 0 or row['no_reads'] > 0 or
#                                                  row['login'] > 0 or row['updates'] > 0 or
#                                                  row['geo'] > 0 or row['no_type'] > 0) else 0, axis=1)
# user_act['write_flag'] = user_act.apply(lambda row: 1 if (row['no_add'] > 0 or row['updates'] > 0) else 0,
#                                                                  axis=1)
#
# user_act.drop_duplicates(subset=['user_code', 'date', 'read_flag'], inplace=True)
#
#
# # In[13]:
#
#
# user_act_all = copy.deepcopy(user_act)
# user_act = user_act[user_act['write_flag'] == 1]
# print(len(set(user_act.user_code)), len(set(user_act_all.user_code)))
#
#
# # In[92]:
#
#
# user_act = user_act.sort_values('dates')
#
#
# # In[ ]:
#
#
# user_act['t'] = user_act['user_code'].astype(str) + user_act['client']
# user_act = user_act.groupby(['t']).head(1)
# user_acts = user_act[['user_code', 'client', 'date']]
# data = list()
#
# for file_name in glob.glob('/home/shreyas/CR-10712/*.json'):
#     with jsonlines.open(file_name) as f:
#         for line in f:
#             client = re.search('/home/shreyas/CR-10712/(.+?)_users.json', file_name).group(1),
#             datum = {
#                 'client': client[0],
#                 'created_date': dt.datetime.strptime(line['date']['$date'], '%Y-%m-%dT%H:%M:%S.%fZ').date() if line.get('date') else np.nan,
#                 'user_code': line['code']
#             }
#             data.append(datum)
# users = pd.DataFrame(data)
#
# users = users[~users['user_code'].isin({'vymoadmin', 'Vymo Agent',
#                                                  'Vymo Admin', 'Vymomanager',
#                                                  'vymoagent', 'SU','Vymo Agent'})]
#
# users = users[~users['created_date'].isna()]
# pd.merge(user_acts, users, on=['client', 'user_code'], how='left')
# old_users = users[users['created_date'].astype(str) < '2019-07-01'] # make it july
# users = users[users['created_date'].astype(str) > '2019-03-31']
# users = users[users['created_date'].astype(str) > '2019-08-31']
# users['created_date'] = pd.to_datetime(users['created_date'])
# users['month'] = users['created_date'].apply(lambda x: mapper[str(x.month)])
# users.groupby('month').agg({'user_code': 'nunique'})
# user_act_main = pd.merge(users, user_acts,on=['user_code', 'client'], how='left')
# user_act_main[~user_act_main['date'].isna()]
# user_act_main = user_act_main[~user_act_main['date'].isna()]
#
# user_act_main['days'] = user_act_main['date'] - user_act_main['created_date']
#
#
# # In[ ]:
#
#
# user_act_main.rename(index=str, columns={"date": "first_update_date"}, inplace=True)
# user_act_main.head()
#
#
# # In[ ]:
#
#
# metrics = user_act_main.groupby(['month', 'days']).agg({'user_code': 'nunique'}).reset_index()
# metrics.rename(index=str, columns={"user_code": "users"}, inplace=True)
# metrics.head()
#
#
# # In[ ]:
#
#
# metrics = metrics[metrics['days'] < '29 days']
#
#
# # In[ ]:
#
#
# metrics.to_csv('Data_Dumps/aggregated.csv', sep=',', encoding='utf-8', index=False)
# user_act_main.to_csv('Data_Dumps/raw.csv',  sep=',', encoding='utf-8', index=False)
#
#
# # In[ ]:
#
#
# metrics = metrics[metrics["month"]== "Sep"]
# metrics
#
#
# # In[ ]:
#
#
# user_act_all.head()
#
#
# # In[ ]:
#
#
# user_act.dtypes
#
#
# # In[24]:
#
#
# old_users
#
#
# # In[25]:
#
#
# old_agg = pd.merge(old_users, user_act_all, on=['client', 'user_code'], how='inner')
# old_agg.head()
#
#
# # In[ ]:
#
#
# old_agg.groupby(['month']).agg({'user_code': 'nunique'})
#
#
# # In[ ]:
#
#
# dau_old = old_agg.groupby(['month', 'date']).agg({'read_flag': np.nansum,
#                                         'write_flag': np.nansum,
#                                         'user_code': 'nunique'}).reset_index()
# dau_old.rename(index=str, columns={"user_code": "users",
#                                    "read_flag": "active",
#                                    "write_flag": "engaged"}, inplace=True)
# dau_old
#
#
# # In[ ]:
#
#
# dau_old.to_csv('Data_Dumps/dau_sept.csv', sep=',', encoding='utf-8', index=False)
#
#
# # In[26]:
#
#
# old_agg[old_agg['write_flag'] > 0].groupby(['month']).agg({'user_code': 'nunique'})
#
#
# # In[27]:
#
#
# old_agg[old_agg['read_flag'] > 0].groupby(['month']).agg({'user_code': 'nunique'})
#
#
# # In[28]:
#
#
# old_agg.groupby(['month']).agg({'user_code': 'nunique'})
#
#
# # In[30]:
#
#
# user_act_filtered = user_act_all[~user_act_all["month"].isin(['Sep', 'Oct'])]
#
#
# # In[33]:
#
#
# all_users = user_act_filtered.user_code.unique()
# # In[34]:
#


user_act_all = pd.read_csv("user_act_all.csv")

x = pd.read_csv('2017_users.csv')
y = pd.read_csv('2018_users.csv')
z = x.append(y)['0'].unique()

user_act_not_oct = user_act_all[~user_act_all["month"].isin(['Oct'])]
user_act_not_oct = user_act_not_oct[user_act_not_oct["no_reads"] > 0]

user_act_filtered = user_act_all[~user_act_all["month"].isin(['Sep', 'Oct'])]
user_act_filtered = user_act_filtered [user_act_filtered["no_reads"] > 0]
# In[74]:


user_act_not_oct.head()
non_oct_users = set(user_act_not_oct["client"].astype(str) + user_act_not_oct["user_code"].astype(str))


# In[75]:


filtered_users = set(user_act_filtered.client.astype(str) + user_act_filtered.user_code.astype(str))


# In[77]:


print(len(set(non_oct_users) - (set(filtered_users) | set(z))))


# # In[56]:
# 
# 
# len(user_act_all[~user_act_all["month"].isin(['Oct'])].user_code.unique())
# 
# 
# reads = user_act_all[user_act_all['read_flag'] == 1]
# active = reads.groupby(['client', 'user_code']).head(1)
# temp = active[['client', 'user_code', 'month', 'year']]
# temp['temp'] = temp['client'].astype(str) + temp['user_code'].astype(str)
# onboard = temp[~temp['temp'].isin(z)].groupby(['temp']).head(1).groupby(['year', 'month']).agg({'temp': 'nunique'}).reset_index()
# onboard
# 
# 
# # In[66]:
# 
# 
# (temp.groupby(['temp']).head(1).groupby(['temp']).size()>1).any()

